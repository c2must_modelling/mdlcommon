from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
import getpass
import os
import platform
import re
import time
import logging
import psutil
import h5py
import glob
import base64
from collections import OrderedDict
from jinja2 import Environment, FileSystemLoader
from pkg_resources import resource_filename

try:
    import cpuinfo
except ImportError:
    _hascpuinfo = False
else:
    _hascpuinfo = True

logger = logging.getLogger(__name__)


def clean_content(content):
    to_pop = []
    for key, val in content.items():
        paths = key.split("/")
        if len(paths) == 2:
            content[paths[0]][paths[-1]] = val
            to_pop.append(key)
        elif len(paths) == 3:
            content[paths[0]][paths[1]][paths[-1]] = val
            to_pop.append(key)
    for key in to_pop:
        content.pop(key)


def prettify(d, indent=0):
    """
    Print the file tree structure with proper indentation.
    """
    text = ""
    nb_items=len(d)
    if nb_items < 6:
        for key, value in d.items():
            if isinstance(value, dict):
                if value["type"] == "group":
                    text += "\t" * indent + "[{}]\n".format(key)
                    text += prettify(value, indent + 1)
                else:
                    text += "\t" * indent + "{} {}\n".format(key, value["shape"])
            else:
                pass
    else:
        key, value = d.popitem()
        key= re.sub(r"\d+", "XXX", key)
        if isinstance(value, dict):
            if value["type"] == "group":
                text += "\t" * indent + "[{}] * ({})\n".format(key, nb_items)
                text += prettify(value, indent + 1)
            else:
                text += "\t" * indent + "{} {} * ({})\n".format(key, value["shape"],nb_items)
        else:
            pass
    return text


def hdf5_read(h5file):
    content = dict()

    def dict_app(key, val):
        val_list = repr(val).split(" ")
        val_type = val_list[1]
        if val_type == "dataset":
            val_shape = " ".join(
                val_list[val_list.index("shape") + 1 : val_list.index("type")]
            )[:-1]
            content[key] = {"type": val_type, "shape": val_shape}
        else:
            content[key] = {"type": val_type}

    with h5py.File(h5file, "r") as f:
        f.visititems(dict_app)

    clean_content(content)
    return prettify(content)


class TemplatedHTMLRepport:
    def __init__(self, manager):
        self.manager = manager

    def build(self):
        if self.manager.username is None:
            self.manager.username = getpass.getuser()
        env = Environment(loader=FileSystemLoader(resource_filename("mdlcommon", ".")))

        template = env.get_template("report_tmpl.html")

        general = OrderedDict()
        general["Model"] = str(self.manager.__module__)
        general["Model Version"] = self.manager.code_version
        general["User"] = self.manager.username
        general["Machine name"] = platform.node()
        general["Time"] = time.strftime("%Hh%M:%S %d/%m/%Y", self.manager.curr_time)
        purpose = self.manager.purpose

        files = sorted(
            filter(
                lambda cfile: not cfile.startswith("."), os.listdir(self.manager.subdir)
            )
        )
        print(self.manager.graphfiles)
        # dgraphs = sorted(
        #     [
        #         dict(
        #             data="data:image/{ext};base64,{b64img}".format(ext=os.path.split(im)[-1].split(".")[-1],b64img=base64.b64encode(open(os.path.join(self.manager.subdir,im),'rb').read())),
        #             name=os.path.split(im)[-1],
        #             ext=os.path.split(im)[-1].split(".")[-1],
        #         )
        #         for im in self.manager.graphfiles
        #     ],
        #     key=lambda x: x["ext"],
        # )
        try:
                dsvg = sorted(
                    [
                        dict(
                            data=open(os.path.join(self.manager.subdir, im), 'r').read(),
                            name=os.path.split(im)[-1],
                            ext=os.path.split(im)[-1].split(".")[-1],
                        )
                        for im in self.manager.graphfiles
                    ],
                    key=lambda x: x["name"],
                )
        except EnvironmentError:
                dsvg = []

        sysname = platform.system()

        if sysname == "Linux":
            osname = " ".join(platform.platform())
            with open("/proc/cpuinfo", "r") as f:
                cpuname = (
                    [line for line in f.readlines() if "model name" in line][0]
                    .split(":")[1]
                    .strip("\n")
                )
        elif sysname == "Windows":
            osname = platform.win32_ver()[0]
            cpuname = platform.processor()
        elif sysname == "Darwin":
            sysname = "MacOS X"
            osname = platform.mac_ver()[0]
            cpuname = platform.processor()
        else:
            osname = ""
            cpuname = platform.processor()

        hard = OrderedDict()
        hard["Operating System:"] = " ".join([sysname, osname])
        hard["Cpu configuration:"] = cpuname
        hard["Total numbers:"] = "{} cores, {} threads".format(
            psutil.cpu_count(False), psutil.cpu_count()
        )
        hard["Total amount of memory:"] = "{:.2f}GB".format(
            psutil.virtual_memory()[0] / (1024 ** 3)
        )

        try:
            h5content = hdf5_read(
                glob.glob(os.path.join(self.manager.subdir, "*.hdf5"))[0]
            )
        except IndexError:
            h5content = ""

        params = list()
        for mdl in self.manager.params_calc:
            if str(mdl.__class__).startswith("<"):
                mname = str(mdl.__class__).split("'")[1]
            else:
                mname = str(mdl.__class__)

            mparams = []
            for k, v in sorted(mdl.parameters.items()):
                try:
                    mparams.append(dict(name=k, value=v, unit=self.manager.units[k]))
                except KeyError:
                    logger.warning(f"Parameter {k}:{v} couldn't be reported")   
            
            params.append({"class": mname, "parameters": mparams})
        try:
            test = dict(BehavoirLaw="Elastic", ElementType="Triangle", k_force=0.001)
            mparams = [
                dict(name=k, value=v, unit=self.manager.units[k])
                for k, v in sorted(test.items())
            ]
            params.append({"class": "FiniteElement", "parameters": mparams})
        except:
            logger.warning("Something went wrong with FE reporting")

        with open(os.path.join(self.manager.subdir, "report.html"), "w") as fhtml:
            fhtml.write(
                template.render(
                    gen=general,
                    pur=purpose,
                    dir=self.manager.subdir,
                    files=files,
                    h5cont=h5content,
                    graphfiles=dsvg,
                    comptime=self.manager.comptime,
                    hard=hard,
                    params_calc=params,
                )
            )
        logger.info("Wrote html report.")
