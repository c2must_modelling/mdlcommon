# coding=utf-8

"""Electrodes grid model.

Provides a simple model to get *n* signals from surface data.
*** Update discarding parallel computing and using indexing instead of multiplications.***

"""

import numpy
import logging
from scipy import special
from scipy import fftpack
import math
from typing import Dict, List, Tuple
from numpy.typing import ArrayLike

try:
    from progressibar2 import Bar, ProgressBar, Percentage
except ImportError:
    HASPBAR = False
else:
    HASPBAR = True

logger = logging.getLogger(__name__)


class Grid(object):
    """Generic Electrode model to generate signals from simulated surfaces."""

    def __init__(
        self,
        surfpot: ArrayLike,
        center: Tuple[int, int],
        delta: Tuple[int, int],
        rotation: int,
        eray: Tuple[int, int],
        nsig: Tuple[int, int] = (8, 8),
        snr: Dict[str, Tuple[int, int]] = {},
    ):
        """
        Creates the electrode model.
        :param surfpot: nX * NY * T surface potentials
        :param center: (Cx,Cy) tuple, grid center in pixels
        :param delta:  (coef_dx, coef_dy) tuple, interElect distance in pixels
        :param rotation: grid rotation in rad
        :param eray: (Rx, Ry) tuple, electrode radius in pixels
        :param nsig: (NsigX,NsigY) tuple, numbers of elects, in both axis
        """

        self.surfpot = surfpot
        self.grid_idx = 0
        self.nbelec = int(nsig[0] * nsig[1])
        self.nsig = nsig
        self.eray = eray
        self.center = center
        self.rotation = rotation
        self.delta = delta
        self.gain_idx = []
        self.snr = snr



        self.sigs = numpy.zeros((self.nbelec, self.surfpot.shape[-1]))
        self.parlist = ["snr", "nbelec", "nsig", "eray", "center", "delta", "rotation"]
        centx, centy = numpy.meshgrid(
            -delta[0] * (nsig[0] - 1) / 2
            + center[0]
            + delta[0] * numpy.arange(nsig[0]),
            -delta[1] * (nsig[1] - 1) / 2
            + center[1]
            + delta[1] * numpy.arange(nsig[1]),
        )

        self.centers_x, self.centers_y = numpy.round(
            (centx - center[0]) * numpy.cos(rotation)
            - (centy - center[1]) * numpy.sin(rotation)
            + center[0]
            + 0.0001
        ), numpy.round(
            (centx - center[0]) * numpy.sin(rotation)
            + (centy - center[1]) * numpy.cos(rotation)
            + center[1]
            + 0.0001
        )

    def bestCandidateCenters(self):
        # en Z (pixel)
        lengthZ = (self.nsig[1] - 1) * self.delta[1] + 2 * self.eray[1]
        # en theta (pixel)
        lengthTheta = (self.nsig[0] - 1) * self.delta[0] + 2 * self.eray[0]

        # tirage d'un premier center aleatoire dans cette surface
        firstCX = numpy.random.randint(
            self.center[0] - round(lengthTheta / 2, 0),
            self.center[0] + round(lengthTheta / 2, 0),
        )
        firstCY = numpy.random.randint(
            self.center[1] - round(lengthZ / 2, 0),
            self.center[1] + round(lengthZ / 2, 0),
        )

        # ajouter a la liste des centres
        self.centers_x = []
        self.centers_y = []
        self.centers_x.append(firstCX)
        self.centers_y.append(firstCY)

        # placer les autres centres d'electrode
        for i in numpy.arange(self.nbelec - 1):
            tmpCX = 0
            tmpCY = 0
            d1 = 0
            # 100 tirages pour avoir le meilleur centre max(min de la distance)
            for j in numpy.arange(100):
                dist = []
                CX = numpy.random.randint(
                    self.center[0] - round(lengthTheta / 2, 0),
                    self.center[0] + round(lengthTheta / 2, 0),
                )
                CY = numpy.random.randint(
                    self.center[1] - round(lengthZ / 2, 0),
                    self.center[1] + round(lengthZ / 2, 0),
                )
                for k in numpy.arange(len(self.centers_x)):
                    d = numpy.sqrt(
                        numpy.power(CX - self.centers_x[k], 2)
                        + numpy.power(CY - self.centers_y[k], 2)
                    )
                    dist.append(d)

                dmin = numpy.min(dist)
                if dmin > d1:
                    for k in numpy.arange(len(self.centers_x)):
                        if CX == self.centers_x[k] and CY == self.centers_y[k]:
                            i = i - 1
                            break
                    tmpCX = CX
                    tmpCY = CY
                    d1 = dmin

            self.centers_x.append(tmpCX)
            self.centers_y.append(tmpCY)

        self.centers_x = numpy.array(self.centers_x)
        self.centers_y = numpy.array(self.centers_y)



    def __repr__(self):
        return (
            "Electrode grid has {} by {} {} electrodes. Electrode radius is {}, angular interelectrode distance"
            " is {} and longitudinal interelectrode distance is {}. Rotation is {}".format(
                self.nsig[0],
                self.nsig[1],
                self.type,
                self.eray,
                self.delta[0],
                self.delta[1],
                180 * self.rotation / numpy.pi,
            )
        )

    def noisy(self):
        try:
            PLI = numpy.sin(2 * numpy.pi * 50 * self.time)
        except AttributeError:
            logger.warning("Time vector isn't set, no PLI Nose computed")
        else:
            try:
                self.snr_PLI = numpy.abs(numpy.random.normal(self.snr['PLI'][0], self.snr['PLI'][1], self.nbelec))
            except KeyError:
                logger.info("No PLI noise values submitted.")
            else:
                self.sigs += (PLI * numpy.mean(numpy.abs(self.sigs) ** 2)) / numpy.sqrt(
                    numpy.mean(numpy.abs(PLI) ** 2)
                    * 10 ** (self.snr_PLI[:, numpy.newaxis] / 10)
                )
        try:
            self.snr_normal = numpy.abs(numpy.random.normal(self.snr['Normal'][0], self.snr['Normal'][1], self.nbelec))
        except KeyError:
            logger.info("No white noise values submitted.")
        else:
            noise = numpy.random.normal(0, 1, self.sigs.shape)
            self.sigs += (noise * numpy.mean(numpy.abs(self.sigs) ** 2)) / numpy.sqrt(
                numpy.mean(numpy.abs(noise) ** 2) * 10 ** (
                        self.snr_normal[:, numpy.newaxis] / 10)
            )
            # normalize to keep snr positive
        try:
            PLI150 = numpy.sin(2 * numpy.pi * 150 * self.time)
        except AttributeError:
            logger.warning("Time vector isn't set, no PLI150 Nose computed")
        else:
            try:
                self.snr_PLI150 = numpy.abs(numpy.random.normal(self.snr['PLI150'][0], self.snr['PLI150'][1], self.nbelec))
            except KeyError:
                logger.info("No PLI150 noise values submitted.")
            else:
                self.sigs += (PLI150 * numpy.mean(numpy.abs(self.sigs) ** 2)) / numpy.sqrt(
                    numpy.mean(numpy.abs(PLI150) ** 2)
                    * 10 ** (self.snr_PLI150[:, numpy.newaxis] / 10)
                )
        try:
            PLI100 = numpy.sin(2 * numpy.pi * 100 * self.time)
        except AttributeError:
            logger.warning("Time vector isn't set, no PLI150 Nose computed")
        else:
            try:
                self.snr_PLI100 = numpy.abs(numpy.random.normal(self.snr['PLI100'][0], self.snr['PLI100'][1], self.nbelec))
            except KeyError:
                logger.info("No PLI100 noise values submitted.")
            else:
                self.sigs += (PLI100 * numpy.mean(numpy.abs(self.sigs) ** 2)) / numpy.sqrt(
                    numpy.mean(numpy.abs(PLI100) ** 2)
                    * 10 ** (self.snr_PLI100[:, numpy.newaxis] / 10)
                )





class GridCirc(Grid):
    """Electrode grid model for circular electrodes."""

    def __init__(
        self, surfpot, center, delta, rotation, eray, nsig=(8, 8), snr={}
    ):
        self.type = "circular"
        Grid.__init__(self, surfpot, center, delta, rotation, eray, nsig, snr)

    def apply(self):
        """Applies electrodes model to surface data."""

        if HASPBAR:
            pbar = ProgressBar(
                widgets=[Percentage(), Bar()], maxval=self.nbelec
            ).start()

        [x, y] = numpy.meshgrid(
            numpy.arange(self.surfpot.shape[0]),
            numpy.arange(self.surfpot.shape[1]),
            indexing="ij",
        )

        for signb in range(int(self.nbelec)):
            G1 = ((x - float(self.centers_x.flatten()[signb])) / self.eray[0]) ** 2 + (
                (y - float(self.centers_y.flatten()[signb])) / self.eray[1]
            ) ** 2 <= 1

            G2 = (
                ((x - float(self.centers_x.flatten()[signb])) / self.eray[0]) ** 2
                + ((y - float(self.centers_y.flatten()[signb])) / self.eray[1]) ** 2
                <= 1 + numpy.exp(1 - self.eray[0] * self.eray[1])
            ) ^ G1

            if G2.sum() != 0:
                self.gain_idx.append(
                    G1 * (G2.sum() - 1) / float(G1.sum() * G2.sum())
                    + G2 / float(G2.sum() ** 2)
                )
                self.sigs[signb] = numpy.sum(self.surfpot[G1], 0) * (
                    (G2.sum() - 1) / float(G1.sum() * G2.sum())
                ) + numpy.sum(self.surfpot[G2], 0) / float(G2.sum() ** 2)
            else:
                self.gain_idx.append(G1 / float(G1.sum()))
                self.sigs[signb] = numpy.sum(self.surfpot[G1], 0) / float(G1.sum())

            if HASPBAR:
                pbar.update(signb + 1)

        if HASPBAR:
            pbar.finish()
        if self.snr:
            self.noisy()

    def transfunc(self, conduct_vol, rayon):
        self.He = numpy.zeros(
            (self.nbelec, conduct_vol.fEMG.shape[0], conduct_vol.fEMG.shape[1]),
            dtype=numpy.complex128,
        )
        Hsize = numpy.zeros(
            (conduct_vol.fEMG.shape[0], conduct_vol.fEMG.shape[1]),
            dtype=numpy.complex128,
        )
        Kth = numpy.arange(
            -conduct_vol.dkth * int(conduct_vol.fEMG.shape[0] / 2),
            conduct_vol.dkth * int(conduct_vol.fEMG.shape[0] / 2) + conduct_vol.dkth,
            conduct_vol.dkth,
            dtype=numpy.float64,
        )
        Kz = numpy.linspace(
            -conduct_vol.dkz * int(conduct_vol.fEMG.shape[1] / 2),
            conduct_vol.dkz * int(conduct_vol.fEMG.shape[1] / 2),
            conduct_vol.fEMG.shape[1],
        )
        self.sigTF = numpy.zeros((self.nbelec, conduct_vol.fEMG.shape[2]))

        Rele = conduct_vol.layers[-1].R / 2.0

        for i in numpy.arange(int(Kth.shape[0] / 2)):
            for j in numpy.arange(int(Kz.shape[0] / 2)):
                if i == 0 and j == 0:
                    Hsize[i, j] = 1
                else:
                    Hsize[i, j] = (
                        2
                        * special.jv(
                            1,
                            rayon
                            * sqrt(
                                pow(Kth[i + int(Kth.shape[0] / 2)] / Rele, 2)
                                + pow(Kz[j + int(Kz.shape[0] / 2)], 2)
                            ),
                        )
                        / (
                            rayon
                            * sqrt(
                                pow(Kth[i + int(Kth.shape[0] / 2)] / Rele, 2)
                                + pow(Kz[j + int(Kz.shape[0] / 2)], 2)
                            )
                        )
                    )

        for i in numpy.arange(int(Kth.shape[0] / 2), Kth.shape[0]):
            for j in numpy.arange(int(Kz.shape[0] / 2)):
                if i == Kth.shape[0] - 1 and j == 0:
                    Hsize[i, j] = 1
                else:
                    Hsize[i, j] = (
                        2
                        * special.jv(
                            1,
                            rayon
                            * sqrt(
                                pow(Kth[i - int(Kth.shape[0] / 2)] / Rele, 2)
                                + pow(Kz[j + int(Kz.shape[0] / 2)], 2)
                            ),
                        )
                        / (
                            rayon
                            * sqrt(
                                pow(Kth[i - int(Kth.shape[0] / 2)] / Rele, 2)
                                + pow(Kz[j + int(Kz.shape[0] / 2)], 2)
                            )
                        )
                    )

        for i in numpy.arange(int(Kth.shape[0] / 2)):
            for j in numpy.arange(int(Kz.shape[0] / 2), Kz.shape[0]):
                if i == 0 and j == Kz.shape[0] - 1:
                    Hsize[i, j] = 1
                else:
                    Hsize[i, j] = (
                        2
                        * special.jv(
                            1,
                            rayon
                            * sqrt(
                                pow(Kth[i + int(Kth.shape[0] / 2)] / Rele, 2)
                                + pow(Kz[j - int(Kz.shape[0] / 2)], 2)
                            ),
                        )
                        / (
                            rayon
                            * sqrt(
                                pow(Kth[i + int(Kth.shape[0] / 2)] / Rele, 2)
                                + pow(Kz[j - int(Kz.shape[0] / 2)], 2)
                            )
                        )
                    )

        for i in numpy.arange(int(Kth.shape[0] / 2), Kth.shape[0]):
            for j in numpy.arange(int(Kz.shape[0] / 2), Kz.shape[0]):
                if i == Kth.shape[0] - 1 and j == Kz.shape[0] - 1:
                    Hsize[i, j] = 1
                else:
                    Hsize[i, j] = (
                        2
                        * special.jv(
                            1,
                            rayon
                            * sqrt(
                                pow(Kth[i - int(Kth.shape[0] / 2)] / Rele, 2)
                                + pow(Kz[j - int(Kz.shape[0] / 2)], 2)
                            ),
                        )
                        / (
                            rayon
                            * sqrt(
                                pow(Kth[i - int(Kth.shape[0] / 2)] / Rele, 2)
                                + pow(Kz[j - int(Kz.shape[0] / 2)], 2)
                            )
                        )
                    )

        for i in numpy.arange(self.nbelec):
            centerZ = self.centers_y.flatten()[i]
            centerTheta = self.centers_x.flatten()[i]
            diracElec = numpy.zeros(
                (conduct_vol.fEMG.shape[0], conduct_vol.fEMG.shape[1])
            )
            diracElec[int(centerTheta), int(centerZ)] = 1.0
            fdecalElec = fftpack.fft2(diracElec)
            self.He[i] = Hsize * fdecalElec
            freqS = conduct_vol.fEMG * self.He[i].reshape(
                conduct_vol.fEMG.shape[0], conduct_vol.fEMG.shape[1], 1
            )
            sig = numpy.sum(numpy.sum(freqS, 0), 0)
            for j in numpy.arange(conduct_vol.Tsimu):
                self.sigTF[
                    i, j * conduct_vol.ft : (j + 1) * conduct_vol.ft
                ] = fftpack.ifft(
                    sig[j * conduct_vol.ft : (j + 1) * conduct_vol.ft]
                ).real


class GridOval(Grid):
    """Electrode grid model for oval electrodes."""

    def __init__(self, surfpot, center, delta, rotation, eray, nsig=(8, 8)):
        self.type = "oval"
        Grid.__init__(self, surfpot, center, delta, rotation, eray, nsig)

    def apply(self):
        """Applies electrodes model to surface data."""

        if HASPBAR:
            pbar = ProgressBar(
                widgets=[Percentage(), Bar()], maxval=self.nbelec
            ).start()
        [x, y] = numpy.meshgrid(
            numpy.arange(self.surfpot.shape[0]),
            numpy.arange(self.surfpot.shape[1]),
            indexing="ij",
        )
        for signb in range(int(self.nbelec)):
            """self.gain_idx.append((((x - float(self.centers_x.flatten()[signb]) - self.eray[0][0]) / self.eray[1][
            0]) ** 2 + ((y - float(self.centers_y.flatten()[signb])) / self.eray[1][1]) ** 2 <= 1) | (((x - float(
            self.centers_x.flatten()[signb]) + self.eray[0][0]) / self.eray[1][0]) ** 2 + ((y - float(
            self.centers_y.flatten()[signb])) / self.eray[1][1]) ** 2 <= 1) | (
                                 (abs(x - float(self.centers_x.flatten()[signb])) <= self.eray[0][0]) & (
                                     abs(y - float(self.centers_y.flatten()[signb])) <= self.eray[0][1])))
            """
            self.gain_idx.append(
                (
                    ((x - float(self.centers_x.flatten()[signb])) / self.eray[1][0])
                    ** 2
                    + (
                        (y - float(self.centers_y.flatten()[signb]) - self.eray[0][0])
                        / self.eray[1][1]
                    )
                    ** 2
                    <= 1
                )
                | (
                    ((x - float(self.centers_x.flatten()[signb])) / self.eray[1][0])
                    ** 2
                    + (
                        (y - float(self.centers_y.flatten()[signb]) + self.eray[0][0])
                        / self.eray[1][1]
                    )
                    ** 2
                    <= 1
                )
                | (
                    (abs(x - float(self.centers_x.flatten()[signb])) <= self.eray[0][0])
                    & (
                        abs(y - float(self.centers_y.flatten()[signb]))
                        <= self.eray[0][1]
                    )
                )
            )
            self.sigs[signb] = (
                numpy.sum(self.surfpot[self.gain_idx[signb]], 0)
                / self.gain_idx[signb].sum()
            )

            if HASPBAR:
                pbar.update(signb + 1)

        if HASPBAR:
            pbar.finish()
        if self.snr:
            noise = numpy.random.normal(0, 1, self.sigs.shape)
            self.sigs += noise * numpy.sqrt(
                numpy.mean(numpy.abs(self.sigs) ** 2)
                / (numpy.mean(numpy.abs(self.noise) ** 2) * 10 ** (self.snr / 10))
            )


class GridConcent(Grid):
    """Electrode grid model for concentric electrodes."""

    def __init__(self, surfpot, center, delta, rotation, eray, nsig=(8, 8)):
        self.type = "concentric"
        Grid.__init__(self, surfpot, center, delta, rotation, eray, nsig)
        self.gain_idx2 = []

    def apply(self):
        """Applies electrodes model to surface data."""

        if HASPBAR:
            pbar = ProgressBar(
                widgets=[Percentage(), Bar()], maxval=self.nbelec
            ).start()
        [x, y] = numpy.meshgrid(
            numpy.arange(self.surfpot.shape[0]),
            numpy.arange(self.surfpot.shape[1]),
            indexing="ij",
        )
        for signb in range(int(self.nbelec)):
            self.gain_idx.append(
                ((x - float(self.centers_x.flatten()[signb])) / self.eray[0][0]) ** 2
                + ((y - float(self.centers_y.flatten()[signb])) / self.eray[0][1]) ** 2
                <= 1
            )
            self.gain_idx2.append(
                (
                    ((x - float(self.centers_x.flatten()[signb])) / self.eray[1][0])
                    ** 2
                    + ((y - float(self.centers_y.flatten()[signb])) / self.eray[1][1])
                    ** 2
                    <= 1
                )
                ^ (
                    ((x - float(self.centers_x.flatten()[signb])) / self.eray[2][0])
                    ** 2
                    + ((y - float(self.centers_y.flatten()[signb])) / self.eray[2][1])
                    ** 2
                    <= 1
                )
            )
            self.sigs[signb] = (
                numpy.sum(self.surfpot[self.gain_idx[signb]], 0)
                / self.gain_idx[signb].sum()
                - numpy.sum(self.surfpot[self.gain_idx2[signb]], 0)
                / self.gain_idx2[signb].sum()
            )

            if HASPBAR:
                pbar.update(signb + 1)

            if HASPBAR:
                pbar.finish()
            if self.snr:
                noise = numpy.random.normal(0, 1, self.sigs.shape)
                self.sigs += noise * numpy.sqrt(
                    numpy.mean(numpy.abs(self.sigs) ** 2)
                    / (numpy.mean(numpy.abs(self.noise) ** 2) * 10 ** (self.snr / 10))
                )
