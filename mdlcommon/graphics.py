from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
# coding=utf-8

"""
Module containing graphical functions for model.
"""

import logging
import numpy
import scipy

import os
try:
    import pyqtgraph as pg
    import pyqtgraph.exporters
    from pyqtgraph.Qt import QtGui
except ImportError:
    _haspg = False
else:
    _haspg = True


try:
    import matplotlib
    import matplotlib.pyplot
    import matplotlib.cm
    import matplotlib.animation as manimation
    from mpl_toolkits.axes_grid1 import Grid # , make_axes_locatable
    #from mpl_toolkits.axes_grid.anchored_artists import AnchoredText

    matplotlib.pyplot.style.use('bmh')
except ImportError:
    _hasmatplot = False
except RuntimeError:
    _hasmatplot = False
else:
    _hasmatplot = True

logger = logging.getLogger(__name__)


def requires_pg(func):
    def wrapper(*args, **kwargs):
        if not _hasmatplot:
            logger.error("pyqtgraph is unavailable for function: " + func.__name__)
        else:
            return func(*args, **kwargs)
    return wrapper


def requires_matplot(func):
    def wrapper(*args, **kwargs):
        if not _hasmatplot:
            logger.error("Matplotlib is unavailable for function: " + func.__name__)
        else:
            return func(*args, **kwargs)

    return wrapper


@requires_matplot
def plot_line(data, name="data", manager=None, export=False, display=True):
    """
    Plot single cell graphs.

    :param data: data numpy array (0D + time)
    :param name: string, name of the data to plot
    :param manager: uemg manager object to provide extra informations
    :param export: bool, export image
    :param display: bool, open a matplotlib figure
    :return:
    """
    fig = matplotlib.pyplot.figure(dpi=150)
    ax = fig.add_subplot(111)
    if manager:
        try:
            ax.plot(manager.tissue.tsave, data)
            ax.set_xlabel("Time ({})".format(manager.units['tmax']))
        except AttributeError:
            ax.plot(data)
            ax.set_xlabel("Time (samples)")
        ax.set_ylabel("{}".format(name))
        fname = os.path.join(manager.subdir, '{}.svg'.format(name))
    else:
        ax.pyplot.plot(data)
        ax.set_xlabel("Time")
        fname = '{}.svg'.format(name)
    if display:
        fig.show()
    if export:
        fig.savefig(fname)
    return ['{}.svg'.format(name)]


@requires_pg
@requires_matplot
def plot_surf_pg(data, name="data", manager=None, export=False, display=True, cmap='viridis'):
    """
    Plots 1D or 2D data over time.

    :param name: name of the data to plot
    :param data: data numpy array (1D or 2D + time)
    :param manager: uemg manager object to provide extra informations
    :param export: bool, export (series of) low-res images
    :param display: bool, open a Qt window with interactif graph
    :param cmap: specify a colormap by its name, among the ones available in matplotlib
    :return:
    """
    if not QtGui.QApplication.instance():
        app = pg.mkQApp()
        localapp = True
    else:
        localapp = False
    if manager:
        xscale, yscale = manager.tissue.cellsize_x, manager.tissue.cellsize_y
        xunit = manager.units['cellsize_x']
        yunit = manager.units['cellsize_y']
        tunit = manager.units['tmax']
        ztime = manager.tissue.tsave
        subdir = os.path.join(manager.subdir, 'tmppng')
    else:
        xscale, yscale = 1., 1.
        xunit = ''
        yunit = ''
        tunit = ''
        ztime = None
        subdir = 'tmppng'
    if len(data.shape) == 3:
        axis_labels = {'bottom': ('X axis', xunit), 'left': ('Y axis', yunit)}
    elif len(data.shape) == 2:
        axis_labels = {'bottom': ('Time', tunit), 'left': ('Y axis', yunit)}
        subdir = os.path.split(subdir)[0]
    else:
        axis_labels = {}

    steps = numpy.linspace(0., 1., 256)
    plt = pg.PlotItem(name=name, labels=axis_labels)
    view = pg.ImageView(view=plt)
    if name == "Stretches":
        cmap = "RdBu_r"
        maxi = abs(data).max()
        view.setImage(data, pos=[0, 0], scale=[xscale, yscale], xvals=ztime, levels=(-maxi, maxi))
    else:
        view.setImage(data, pos=[0, 0], scale=[xscale, yscale], xvals=ztime)
    #matplotlib.pyplot.setAspectLocked(False)

    try:
        cmap_plt = matplotlib.pyplot.get_cmap(cmap)
    except ValueError:
        cmap_plt = matplotlib.pyplot.get_cmap('jet')

    clrmap_pg = pg.ColorMap(steps, cmap_plt(steps))
    view.ui.histogram.gradient.setColorMap(clrmap_pg)
    if display:
        view.show()
        app.exec_()
    fname = os.path.join(subdir, 'myometrium_{}.png'.format(name))
    if export:
        print("Exporting")
        view.export(fname)

    if localapp:
        app.quit()

    return fname


# @requires_matplot
# def vid_surf(data, name="data", manager=None, cmap='viridis'):
#
#     xscale, yscale = manager.tissue.cellsize_x, manager.tissue.cellsize_y
#     xunit = manager.units['cellsize_x']
#     yunit = manager.units['cellsize_y']
#     tunit = manager.units['tmax']
#     stime = manager.tissue.tsave
#     # subdir = os.path.join(manager.subdir, 'tmppng')
#
#     freq = int(1000. / numpy.diff(stime).mean())
#     ratio = data.shape[1] / data.shape[2]
#
#     FFMpegWriter = manimation.writers['ffmpeg']
#     metadata = dict(title='Movie Test', artist='Matplotlib',
#                     comment='Movie support!')
#     writer = FFMpegWriter(fps=freq, codec='h264', metadata=metadata)
#
#     # axis_labels = {'bottom': ('X axis', xunit), 'left': ('Y axis', yunit)}
#
#     if name == "Stretches":
#         cmap = "RdBu_r"
#         maxi = abs(data).max()
#         mini = -maxi
#     else:
#         maxi = data.max()
#         mini = data.min()
#
#     fig = matplotlib.pyplot.figure(figsize=(4. * ratio, 4.), dpi=100)
#     ax = fig.add_subplot(111)
#     ax.set_title(" ".join(name.split('_')[:2]))
#     ax.set_xlabel("X ({})".format(xunit))
#     ax.set_ylabel("Y ({})".format(yunit))
#     divider = make_axes_locatable(ax)
#     cax = divider.append_axes("right", size="5%", pad=0.05)
#     img = ax.imshow(data[0], extent=[0, data.shape[1] * xscale, 0, data.shape[2] * yscale], vmin=mini,
#                                    vmax=maxi, aspect='equal', origin='lower', cmap=matplotlib.pyplot.get_cmap(cmap))
#     matplotlib.pyplot.colorbar(img,cax=cax)
#     with writer.saving(fig, os.path.join(manager.subdir, name), 100):
#         for idx in range(data.shape[0]):
#             img.set_data(data[idx])
#             writer.grab_frame()
#
#     return name

@requires_matplot
def plot_grid(grid, subdir=''):
    """
    Plot the 2d images of the electrode grid with numbered channels.
    :param grid:
    """
    fig2 = matplotlib.pyplot.figure(figsize=(12, 8), dpi=150)
    matplotlib.pyplot.title("Sum of electrodes functions.")
    try:
        matplotlib.pyplot.imshow(numpy.sum(grid.gain_idx, axis=0)-numpy.sum(grid.gain_idx2, axis=0), origin='lower')
    except AttributeError:
        matplotlib.pyplot.imshow(numpy.sum(grid.gain_idx, axis=0), origin='upper')  # , extent=[-grid.zmax, grid.zmax, -90, 90])

    # matplotlib.pyplot.xlabel('z (mm)')
    # matplotlib.pyplot.ylabel(r'$\theta$ ($\degree$)')

    for idx in numpy.arange(int(grid.nbelec)):
        matplotlib.pyplot.text(grid.centers_y.flatten()[idx], grid.centers_x.flatten()[idx], "%d" % idx,
                               horizontalalignment='center', verticalalignment='center', color="white")
    file_grid = 'ElectrodeGrid{}.svg'.format(grid.grid_idx)
    try:
        matplotlib.pyplot.savefig(subdir / file_grid)
    except TypeError:
        matplotlib.pyplot.savefig(subdir+file_grid)
    matplotlib.pyplot.close("all")
    return [file_grid]


@requires_matplot
def plot_grid_u(grid, id_grid=0, subdir=None, manager=None):
    """
    Plot the 2d images of the electrode grid with numbered channels.
    :param grid:
    """

    if manager:
        subdir = manager.subdir
        xscale, yscale = manager.tissue.cellsize_x * manager.tissue.shape[0], manager.tissue.cellsize_y * \
                         manager.tissue.shape[1]
        xunit = manager.units['cellsize_x']
        yunit = manager.units['cellsize_y']
    else:
        if not subdir:
            subdir = ''
        xscale, yscale = numpy.sum(grid.gain_idx, axis=0).shape
        xunit = 'px'
        yunit = 'px'

    fig = matplotlib.pyplot.figure(figsize=(12, 8), dpi=150)
    matplotlib.pyplot.title("Sum of electrodes functions.")
    cmaplist=matplotlib.colors.ListedColormap([col['color'] for col in matplotlib.pyplot.rcParams['axes.prop_cycle']][:2])
    try:
        matplotlib.pyplot.imshow(numpy.sum(grid.gain_idx, axis=0) - numpy.sum(grid.gain_idx2, axis=0), origin='lower',
                                 extent=[0, xscale, 0, yscale], cmap=cmaplist)
    except AttributeError:
        matplotlib.pyplot.imshow(numpy.sum(grid.gain_idx, axis=0), origin='lower', extent=[0, xscale, 0, yscale],
                                 cmap=cmaplist)

    matplotlib.pyplot.xlabel('x ({})'.format(xunit))
    matplotlib.pyplot.ylabel('y ({})'.format(yunit))
    for idx in numpy.arange(int(grid.nbelec)):
        matplotlib.pyplot.text(grid.centers_y.flatten()[idx] * manager.tissue.cellsize_y,
                               grid.centers_x.flatten()[idx] * manager.tissue.cellsize_x, "%d" % idx,
                               horizontalalignment='center', verticalalignment='center', color="white")
    file_grid = 'ElectrodeGrid_{}'.format(id_grid)
    matplotlib.pyplot.savefig(subdir + file_grid + '.svg')
    matplotlib.pyplot.savefig(subdir + file_grid + '.png')
    matplotlib.pyplot.close("all")
    return file_grid + '.png'

@requires_matplot
def plot_emg(grid, mask=None, subdir=None):
    """
    Plots simulated EMG signals

    :param grid:
    :param mask: Optional convolution mask, generate a second plot with the composite signals
    :return: void
    """

    if grid.nsig[0] == 1 or grid.nsig[1] == 1:
        fig = matplotlib.pyplot.figure(figsize=(8, 8), dpi=150)
        plgrid = Grid(fig, rect=111, nrows_ncols=grid.nsig.astype(int),
                      axes_pad=0.25, label_mode='L', )
        k = 0
        idlist = numpy.arange(grid.nbelec, dtype='int32').reshape(grid.nsig)[:, ::-1].T.flatten()
        for ax in plgrid:
            ax.plot(grid.sigs[idlist[k]])
            # ax.set_ylabel("{:.1f}mm".format((grid.centersGz.flat[idlist[k]] - grid.GE.shape[1] / 2) * grid.hZ),
            # fontsize=10)
            # ax.set_xlabel("{}$\degree$".format((grid.centersGth.flat[idlist[k]] - grid.GE.shape[0] / 2) * grid.hTh),
            #              fontsize=10)
            ax.set_ylim([grid.sigs.min(), grid.sigs.max()])

            ax.title.set_visible(False)
            ax.tick_params(labelsize=8)

            k += 1
    else:
        fig = matplotlib.pyplot.figure(figsize=(12, 8), dpi=150)
        plgrid = Grid(fig, rect=111, nrows_ncols=grid.nsig.astype(int),
                      axes_pad=0.25, label_mode='L', )
        k = 0
        idlist = numpy.arange(grid.nbelec, dtype='int32').reshape(grid.nsig.astype(int))[:, ::-1].T.flatten()
        for ax in plgrid:
            ax.plot(grid.sigs[idlist[k]])
            # ax.set_xlabel("{:.1f}mm".format((grid.centersGz.flat[idlist[k]] - grid.GE.shape[1] / 2) * grid.hZ),
            # fontsize=10)
            # ax.set_ylabel("{}$\degree$".format((grid.centersGth.flat[idlist[k]] - grid.GE.shape[0] / 2) * grid.hTh),
            #               fontsize=10)
            ax.set_ylim([grid.sigs.min(), grid.sigs.max()])

            ax.title.set_visible(False)
            ax.tick_params(labelsize=8)

            k += 1
    file_mono = 'Monopolar_EMG_{}.svg'.format(grid.grid_idx)  # grid should include its number !!!
    flist = [file_mono]
    matplotlib.pyplot.tight_layout()
    try:
        matplotlib.pyplot.savefig(subdir / file_mono)
    except TypeError:
        matplotlib.pyplot.savefig(subdir+file_mono)
    #matplotlib.pyplot.savefig(subdir+file_mono+'.png')

    if mask:
        fig3 = matplotlib.pyplot.figure(figsize=(12, 8), dpi=150)
        with open(subdir+'Composite_mask_{}.txt'.format(0),'w') as f:
            f.write(repr(mask))
        mask = numpy.array(mask, ndmin=3).T
        msigs = scipy.signal.convolve(grid.sigs.reshape(grid.nsig[0].astype(int), grid.nsig[1].astype(int), grid.sigs.shape[-1]), mask, mode='valid')
        mnsig = (msigs.shape[0], msigs.shape[1])
        msigs = msigs.reshape(msigs.shape[0]*msigs.shape[1], grid.sigs.shape[-1])
        plgrid = Grid(fig3, rect=111, nrows_ncols=mnsig,
                          axes_pad=0.25, label_mode='L', )
        k = 0
        idlist = numpy.arange(mnsig[0]*mnsig[1], dtype='int32').reshape(mnsig)[:, ::-1].T.flatten()
        for ax in plgrid:
            ax.plot(msigs[idlist[k]])
            # ax.set_xlabel("{:.1f}mm".format((grid.centersGz.flat[idlist[k]] - grid.GE.shape[1] / 2) * grid.hZ),
            # fontsize=10)
            # ax.set_ylabel("{}$\degree$".format((grid.centersGth.flat[idlist[k]] - grid.GE.shape[0] / 2) * grid.hTh),
            #               fontsize=10)
            ax.set_ylim([msigs.min(), msigs.max()])

            ax.title.set_visible(False)
            ax.tick_params(labelsize=8)

            k += 1
        matplotlib.pyplot.tight_layout()
        file_masked = 'Composite_EMG_{}.svg'.format(grid.grid_idx)  # grid should include its number !!!
        flist.append(file_masked)
        try :
            matplotlib.pyplot.savefig(subdir / file_masked)
        except TypeError:
            matplotlib.pyplot.savefig(subdir+file_masked)
    matplotlib.pyplot.close("all")
    return flist

#@requires_matplot
# def plot_emg_u(grid, id_grid=0, manager=None):
#     """
#     Plots simulated EMG signals
#
#     :param grid:
#     :return: void
#     """
#
#     if manager:
#         subdir = manager.subdir
#         time = manager.tissue.tsave / 1e3
#     else:
#         subdir = ''
#         time = numpy.arange(grid.sigs.shape[1])
#
#     if grid.nsig[0] == 1 or grid.nsig[1] == 1:
#         fig = matplotlib.pyplot.figure(figsize=(max(8,int(2*numpy.sqrt(grid.nbelec))),8), dpi=150)
#         plgrid = Grid(fig, rect=111, nrows_ncols=grid.nsig.astype(int),
#                       axes_pad=0.25, label_mode='L', )
#         idlist = numpy.arange(grid.nbelec, dtype='int32').reshape(grid.nsig)[:, ::-1].T.flatten()
#         for k, ax in enumerate(plgrid):
#             ax.plot(time, grid.sigs[idlist[k]])
#             ax.set_ylim([grid.sigs.min(), grid.sigs.max()])
#             ax.set_xlabel("time (s)")
#             ax.set_ylabel("EMG (mV)")
#             ax.title.set_visible(False)
#             ax.tick_params(labelsize=8)
#             at = AnchoredText("{}".format(idlist[k]), frameon=True, loc=3)
#             at.patch.set_boxstyle("numpy.round,pad=0.,numpy.rounding_size=0.2")
#             ax.add_artist(at)
#     else:
#         fig = matplotlib.pyplot.figure(figsize=(max(12,int(3*numpy.sqrt(grid.nbelec))), max(8,int(2*numpy.sqrt(grid.nbelec)))), dpi=150)
#         plgrid = Grid(fig, rect=111, nrows_ncols=grid.nsig.astype(int),
#                       axes_pad=0.25, label_mode='L', )
#
#         idlist = numpy.arange(grid.nbelec, dtype='int32').reshape(grid.nsig)[:, ::-1].T.flatten()
#         for k, ax in enumerate(plgrid):
#             ax.plot(time, grid.sigs[idlist[k]])
#             ax.set_ylim([grid.sigs.min(), grid.sigs.max()])
#             ax.set_xlabel("time (s)")
#             ax.set_ylabel("EMG (mV)")
#             ax.title.set_visible(False)
#             ax.tick_params(labelsize=8)
#             at = AnchoredText("{}".format(idlist[k]), frameon=True, loc=3)
#             at.patch.set_boxstyle("numpy.round,pad=0.,numpy.rounding_size=0.2")
#             ax.add_artist(at)
#
#     file_mono = 'Monopolar_EMG_{}'.format(id_grid)  # grid should include its number !!!
#     matplotlib.pyplot.tight_layout()
#     matplotlib.pyplot.savefig(subdir + file_mono + '.svg')
#     matplotlib.pyplot.savefig(subdir + file_mono + '.png')
#     matplotlib.pyplot.close("all")
#     return file_mono + '.png'


def plot_MUrecrut(nUM, RTE, manager=None):
    mu_recrut = numpy.zeros(nUM)
    mvc = numpy.arange(nUM, dtype=numpy.float64)/nUM * 100
    if manager:
        subdir = str(manager.subdir)+'/'
    else:
        subdir = ''
    for pMVC in numpy.arange(nUM):
        for UM in numpy.arange(nUM):
            if mvc[pMVC] >= RTE[UM]:
                mu_recrut[pMVC] += 1

    matplotlib.pyplot.plot(mvc, mu_recrut)
    matplotlib.pyplot.savefig(subdir+"mu_recrut.svg")
    matplotlib.pyplot.close("all")

    return ["mu_recrut.svg"]



@requires_matplot
def plot_IUP(IUP, manager=None):
    if manager:
        subdir = str(manager.subdir)+'/'
        time = manager.tissue.tsave / 1e3
    else:
        subdir = ''
        time = numpy.arange(IUP.shape[0])
    matplotlib.pyplot.figure(dpi=150)
    matplotlib.pyplot.rc('xtick' )
    matplotlib.pyplot.rc('ytick')
    matplotlib.pyplot.figure()
    matplotlib.pyplot.plot(time, IUP*7.5600617, 'k', linewidth=3.0)
    matplotlib.pyplot.ylabel('IUP (mmHg)' )
    matplotlib.pyplot.xlabel('Time (s)')
    matplotlib.pyplot.legend(['Simulated IUP'], loc=4)

    file_iup = 'IUP'
    matplotlib.pyplot.savefig(subdir + file_iup + '.svg')
    #matplotlib.pyplot.savefig(subdir + file_iup + '.png')
    matplotlib.pyplot.close("all")
    return [file_iup + '.svg']

@requires_matplot
def plot_survey(signals, manager=None):
    if manager:
        subdir = str(manager.subdir)+'/'
        time = manager.tissue.tsave / 1e3
        xlbl = 'Time (s)'
    else:
        subdir = ''
        xlbl = 'Samples'
    matplotlib.pyplot.figure(dpi=150)
    matplotlib.pyplot.title("Survey of simulation outputs.")
    matplotlib.pyplot.rc('xtick')
    matplotlib.pyplot.rc('ytick')
    for idx, signal in enumerate(signals):
        matplotlib.pyplot.subplot(len(signals), 1, idx+1)
        try:
            len(time)
        except NameError:
            time = numpy.arange(len(signal['data']))
        if signal['name'] == 'IUP':
            factor = 7.5600617
        else:
            factor = 1
        matplotlib.pyplot.plot(time, signal['data']*factor, 'k', linewidth=3.0)
        matplotlib.pyplot.ylabel(f"{signal['name']} ({signal['unit']})")
        matplotlib.pyplot.xlabel(xlbl)
        #matplotlib.pyplot.legend(['Simulated IUP'], loc=4)

    file_iup = 'Sim_survey'
    matplotlib.pyplot.savefig(subdir + file_iup + '.svg')
    #matplotlib.pyplot.savefig(subdir + file_iup + '.png')
    matplotlib.pyplot.close("all")
    return [file_iup + '.svg']