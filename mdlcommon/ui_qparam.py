# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created: Thu Jun 13 11:25:50 2013
# by: pyside-uic 0.2.14 running on PySide 1.1.2
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(752, 600)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.layoutWidget = QtGui.QWidget(self.centralwidget)
        self.layoutWidget.setGeometry(QtCore.QRect(20, 20, 320, 25))
        self.layoutWidget.setObjectName("layoutWidget")
        self.horizontalLayout = QtGui.QHBoxLayout(self.layoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.newButton = QtGui.QPushButton(self.layoutWidget)
        self.newButton.setObjectName("newButton")
        self.horizontalLayout.addWidget(self.newButton)
        self.openButton = QtGui.QPushButton(self.layoutWidget)
        self.openButton.setObjectName("openButton")
        self.horizontalLayout.addWidget(self.openButton)
        self.saveButton = QtGui.QPushButton(self.layoutWidget)
        self.saveButton.setObjectName("saveButton")
        self.horizontalLayout.addWidget(self.saveButton)
        self.addMButton = QtGui.QPushButton(self.layoutWidget)
        self.addMButton.setObjectName("addMButton")
        self.horizontalLayout.addWidget(self.addMButton)
        self.addButton = QtGui.QPushButton(self.layoutWidget)
        self.addButton.setObjectName("addButton")
        self.horizontalLayout.addWidget(self.addButton)
        self.modifyButton = QtGui.QPushButton(self.layoutWidget)
        self.modifyButton.setObjectName("modifyButton")
        self.horizontalLayout.addWidget(self.modifyButton)
        self.deleteButton = QtGui.QPushButton(self.layoutWidget)
        self.deleteButton.setObjectName("deleteButton")
        self.horizontalLayout.addWidget(self.deleteButton)
        self.treeview = QtGui.QTreeWidget(self.centralwidget)
        self.treeview.setHeaderLabels(('Param', 'Unit', 'Value'))
        self.treeview.setMinimumSize(QtCore.QSize(711, 0))
        self.verticalLayout.addWidget(self.layoutWidget)
        self.verticalLayout.addWidget(self.treeview)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(QtGui.QApplication.translate("MainWindow", "Main", None,
                                                               QtGui.QApplication.UnicodeUTF8))
        self.newButton.setText(QtGui.QApplication.translate("MainWindow", "New", None, QtGui.QApplication.UnicodeUTF8))
        self.openButton.setText(QtGui.QApplication.translate("MainWindow", "Open", None,
                                                             QtGui.QApplication.UnicodeUTF8))
        self.saveButton.setText(QtGui.QApplication.translate("MainWindow", "Save", None,
                                                             QtGui.QApplication.UnicodeUTF8))
        self.deleteButton.setText(QtGui.QApplication.translate("MainWindow", "Delete", None,
                                                               QtGui.QApplication.UnicodeUTF8))
        self.addMButton.setText(
            QtGui.QApplication.translate("MainWindow", "+Mdl", None, QtGui.QApplication.UnicodeUTF8))
        self.addButton.setText(
            QtGui.QApplication.translate("MainWindow", "+Param", None, QtGui.QApplication.UnicodeUTF8))
        self.modifyButton.setText(QtGui.QApplication.translate("MainWindow", "Mod. Par", None,
                                                               QtGui.QApplication.UnicodeUTF8))


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setWindowModality(QtCore.Qt.ApplicationModal)
        Dialog.resize(230, 204)
        self.buttonBox = QtGui.QDialogButtonBox(Dialog)
        self.buttonBox.setGeometry(QtCore.QRect(37, 160, 156, 23))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel | QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.formLayoutWidget = QtGui.QWidget(Dialog)
        self.formLayoutWidget.setGeometry(QtCore.QRect(30, 20, 171, 126))
        self.formLayoutWidget.setObjectName("formLayoutWidget")
        self.formLayout = QtGui.QFormLayout(self.formLayoutWidget)
        self.formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setContentsMargins(0, 0, 0, 0)
        self.formLayout.setObjectName("formLayout")
        self.label_Nom = QtGui.QLabel(self.formLayoutWidget)
        self.label_Nom.setObjectName("label_Nom")
        self.formLayout.setWidget(0, QtGui.QFormLayout.LabelRole, self.label_Nom)
        self.comboBox_Nom = QtGui.QComboBox(self.formLayoutWidget)
        self.comboBox_Nom.setObjectName("comboBox_Nom")
        self.formLayout.setWidget(0, QtGui.QFormLayout.FieldRole, self.comboBox_Nom)
        self.label_Type = QtGui.QLabel(self.formLayoutWidget)
        self.label_Type.setObjectName("label_Type")
        self.formLayout.setWidget(1, QtGui.QFormLayout.LabelRole, self.label_Type)
        self.comboBox_Type = QtGui.QComboBox(self.formLayoutWidget)
        self.comboBox_Type.setObjectName("comboBox_Type")
        self.formLayout.setWidget(1, QtGui.QFormLayout.FieldRole, self.comboBox_Type)
        self.label_P1 = QtGui.QLabel(self.formLayoutWidget)
        self.label_P1.setObjectName("label_P1")
        self.formLayout.setWidget(2, QtGui.QFormLayout.LabelRole, self.label_P1)
        self.lineEdit_P1 = QtGui.QLineEdit(self.formLayoutWidget)
        self.lineEdit_P1.setInputMethodHints(QtCore.Qt.ImhFormattedNumbersOnly)
        self.lineEdit_P1.setObjectName("lineEdit_P1")
        self.formLayout.setWidget(2, QtGui.QFormLayout.FieldRole, self.lineEdit_P1)

        self.retranslateUi(Dialog)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), self.add_param)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), Dialog.reject)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QtGui.QApplication.translate("Dialog", "Parameters", None,
                                                           QtGui.QApplication.UnicodeUTF8))
        self.label_Nom.setText(QtGui.QApplication.translate("Dialog", "Name", None, QtGui.QApplication.UnicodeUTF8))
        self.label_Type.setText(QtGui.QApplication.translate("Dialog", "Unit", None, QtGui.QApplication.UnicodeUTF8))
        self.label_P1.setText(QtGui.QApplication.translate("Dialog", "Value", None, QtGui.QApplication.UnicodeUTF8))

