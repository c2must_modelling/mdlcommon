# coding=utf-8
""" **Common parts of muscle models**"""
try:
    from setuptools import setup, find_packages

    setuptools = True
except ImportError as err:
    from distutils.core import setup

    setuptools = False

setup(
    name='mdlcommon',
    version='1.1',
    author='J. Laforet',
    author_email='jlaforet@utc.fr',
    packages=find_packages(),
    package_data={'mdlcommon': ["css/*", "*.html"]},
    license='LICENSE.txt',
    url='http://pypi.python.org/pypi/mdlcommon/',
    description='Uterine EMG simulator.',
    long_description="",
    install_requires=[
        "numpy", "scipy", 'astropy', 'h5py', 'jsonschema', "matplotlib", "progressbar2", 'psutil', 'jinja2'
    ],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Science/Research",
        "Topic :: Scientific/Engineering :: Medical Science Apps.",
        "License :: OSI Approved :: GNU General Public License v2 (GPLv2)"
    ]
)
